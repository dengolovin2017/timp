﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessFunctions
{
    class PieceOnCell //фигура стоящая на клетке
    {
        public Piece piece { get; private set; }
        public Cell cell { get; private set; }

        public PieceOnCell(Piece piece, Cell cell) 
        {
            this.piece = piece;
            this.cell = cell;
        }

    }
}
