﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessFunctions
{
    struct Cell //Ячейка
    {
        public static Cell none = new Cell(-1, -1); //Ячейка при ошибке
        public int x { get; private set; } 
        public int y { get; private set; }
        
        public Cell(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
 

        public Cell(string a0) //конструктор для ввода текстом h2, e2, e4
        {
            if (a0.Length == 2 
                && a0[0] >= 'a' && a0[0] <= 'h'
                && a0[1] >= '1' && a0[1] <= '8')
            {
                x = a0[0] - 'a';
                y = a0[1] - '1';
            } else
            {
                this = none;
            }
        }

        public bool onBoard() //проверка нахождения ячейки на доске
        {
            return (x >= 0 && x < 8 && y >= 0 && y < 8);
        }

        public static bool operator == (Cell a, Cell b) //операторы сравнения клеток
        {
            return a.x == b.x && a.y == b.y;
        }

        public static bool operator != (Cell a, Cell b)
        {
            return a.x != b.x || a.y != b.y;
        }

        public static IEnumerable<Cell> EnumCells()
        {
            for (int y = 0; y < 8; y++)
                for (int x = 0; x < 8; x++)
                    yield return new Cell(x, y);   
        }

        public string Name { get { return ((char)('a' + x)).ToString() + (y + 1).ToString(); } }
    }
}
