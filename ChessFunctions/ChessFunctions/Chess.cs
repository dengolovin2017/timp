﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessFunctions
{
    public class Chess  
    {
        public string fen; //"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
        Board board;
        Move moves;
        List<PieceMoving> allMoves;

        public Chess(string fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
        {   //конструктор для подачи fen
            this.fen = fen;
            board = new Board(fen);
            moves = new Move(board);
           
        }

        Chess (Board board) //конструктор для обновления доски
        {
            this.board = board;
            this.fen = board.fen; //задание fen шахматам от доски
            moves = new Move(board);
        }

        public Chess Move(string move) //Ход
        {

            PieceMoving pm = new PieceMoving(move); //Создание хода
            if (!moves.canMove(pm)) //Если ход неверный, то вернем начальные позиции
                return this;
            if (board.IsCkeckAfterMove(pm))
                return this;
            Board nextBoard;
            Chess nextChess;
            if (moves.CanPawnMoveProhod())
            {
                if (pm.piece.GetColor() == Color.white)
                {
                    nextBoard = board.Move(pm, new Cell(pm.to.x, pm.to.y - 1));  
                }
                else
                {
                    nextBoard = board.Move(pm, new Cell(pm.to.x, pm.to.y + 1));
                }
                nextChess = new Chess(nextBoard);
                return nextChess;
            } 

            nextBoard = board.Move(pm); //Создание новой доски на основе хода
            nextChess = new Chess(nextBoard); //Создание новой партии (снапшот) на основе созданной доски
            return nextChess; 
        }

        public char GetPieceAt (int x, int y) //Получение Фигуры по координатам не зная фигуры
        {
            Cell cell = new Cell(x, y); 
            Piece p = board.GetPieceAt(cell); 
            return p == Piece.none ? '.' : (char) p;
        }

        public char GetPieceAt(string position) //Получение Фигуры по координатам не зная фигуры
        {
            Cell cell = new Cell(position);
            Piece p = board.GetPieceAt(cell);
            return p == Piece.none ? '.' : (char)p;
        }

        public void FindAllMoves() //поиск всех ходов
        {
            allMoves = new List<PieceMoving>();
            foreach (PieceOnCell pc in board.EnumPieces())
                foreach (Cell to in Cell.EnumCells())
                {
                    PieceMoving pm = new PieceMoving(pc, to);
                    if (moves.canMove(pm))
                        if (!board.IsCkeckAfterMove(pm))
                            allMoves.Add(pm);
                }
        }

        public List<string> GetAllMoves()
        {
            FindAllMoves();
            List<string> list = new List<string>();

            foreach (PieceMoving pm in allMoves)
                list.Add(pm.ToString());
            return list;
        }

        public bool IsCheck()
        {
            return board.IsCheck();
        }

        public bool IsCheckMAT()
        {
            return GetAllMoves().Count == 0 && board.IsCheck();
        }

        public bool IsCheckPAT()
        {
            return GetAllMoves().Count == 0 && !board.IsCheck();
        }

    }
}
