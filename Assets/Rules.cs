﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChessFunctions;
using System;

public class Rules : MonoBehaviour
{
    Transfer t;
    Chess chess;

    public Rules()
    {
        t = new Transfer();
        chess = new Chess();
        
    }

    // Start is called before the first frame update
    public void Start()
    {
        ShowPieces();
        MarkValidPieces();
    }

    // Update is called once per frame
    void Update()
    {
        if (t.Action())
        {
            string from = GetCell(t.pickPosition);
            string to = GetCell(t.dropPosition);
            string piece = chess.GetPieceAt(from).ToString();
            string move = piece + from + to;

            if (chess != chess.Move(move))
            {
                chess = chess.Move(move); 
                Debug.Log(move);
            } else
            {
                Debug.Log(move + " - Ход неверный!");
            }
            ShowPieces();
            MarkValidPieces();
        }
    }

    string GetCell(Vector2 position)
    {
        int x = Convert.ToInt32(position.x / 2.0);
        int y = Convert.ToInt32(position.y / 2.0);

        return ((char)('a' + x)).ToString() + (y + 1).ToString();
    }


    void ShowPieces()
    {
        int count = 0;
        for (int y = 0; y < 8; y++)
            for (int x = 0; x < 8; x++)
            {
                string piece = chess.GetPieceAt(x, y).ToString();
                if (piece == ".") continue;
                PlacePiece("box" + count, piece, x, y);
                count++;
            }
        for (; count < 32; count++)
            PlacePiece("box" + count, "q", 9, 9);
    }

    void MarkValidPieces()
    {
        for (int y = 0; y < 8; y++)
            for (int x = 0; x < 8; x++)
                MarkCell(x, y, false);
        foreach (string moves in chess.GetAllMoves())
        {
            int x, y;
            GetCoordinate(moves.Substring(1, 2), out x, out y);
            MarkCell(x, y, true);
        }
    }

    void GetCoordinate(string name, out int x, out int y)
    {
        x = 9;
        y = 9;
        if (name.Length == 2 &&
            name[0] >= 'a' && name[0] <= 'h' &&
            name[1] >= '1' && name[1] <= '8')
        {
            x = name[0] - 'a';
            y = name[1] - '1';
        }
    }

    void PlacePiece(string box, string piece, int x, int y)
    {
        GameObject goBox = GameObject.Find(box);
        GameObject goPiece = GameObject.Find(piece);
        GameObject goCell = GameObject.Find("" + y + x);

        var spritePiece = goPiece.GetComponent<SpriteRenderer>();
        var spriteBox = goBox.GetComponent<SpriteRenderer>();
        spriteBox.sprite = spritePiece.sprite;

        goBox.transform.position = goCell.transform.position;
    }

    void MarkCell(int x, int y, bool isMarked)
    {
        GameObject goCell = GameObject.Find("" + y + "" + x);
        GameObject cell;
        string color = (x + y) % 2 == 0 ? "Black" : "White";
        if (isMarked)
            cell = GameObject.Find(color + "SquareMarked");
        else
            cell = GameObject.Find(color + "Square");

        var spriteCell = goCell.GetComponent<SpriteRenderer>();
        var spriteCell2 = cell.GetComponent<SpriteRenderer>();

        spriteCell.sprite = spriteCell2.sprite;
    } 
}

class Transfer
{
    enum State
    {
        none,
        drag
    }

    public Vector2 pickPosition { get; private set; }
    public Vector2 dropPosition { get; private set; }
    State state;
    GameObject item;
    Vector2 offset;
    
    public Transfer()
    {
        state = State.none;
        item = null;
    }

    public bool Action()
    {
        switch (state)
        {
            case State.none:
                if (IsMouseButtonPressed())
                    PickUp();
                break;
            case State.drag:
                if (IsMouseButtonPressed())
                    Drag();
                else
                {
                    Drop();
                    return true;
                }
                break;
        }
        return false;
    }

    bool IsMouseButtonPressed()
    {
        return Input.GetMouseButton(0);
    }

    void PickUp()
    {
        Vector2 clickPosition = GetClickPosition();
        Transform clickedItem = GetItemAt(clickPosition);
        if (clickedItem == null) return;
        pickPosition = clickedItem.position;
        item = clickedItem.gameObject;
        state = State.drag;
        offset = pickPosition - clickPosition;
    }

    Vector2 GetClickPosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    Transform GetItemAt(Vector2 position)
    {
        RaycastHit2D[] pieces = Physics2D.RaycastAll(position, position, 0.5f);
        if (pieces.Length == 0)
            return null;
        return pieces[0].transform;
    }

    void Drag()
    {
        item.transform.position = GetClickPosition() + offset;
    }

    void Drop()
    {
        dropPosition = item.transform.position;
        state = State.none;
        item = null;
    }
}
