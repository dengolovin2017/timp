﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChessApi_server.Models;
using ChessFunctions;

namespace ChessApi_server.Services
{
    public class Logic
    {
        
        private ModelChess db;

        public Logic()
        {
            db = new ModelChess();
        }

        public Game GetCurrentGame()
        {
            Game game = db
                .Games
                .Where(g => g.status == "play")
                .OrderByDescending(g => g.id)
                .FirstOrDefault();
            if (game == null)
                game = CreateNewGame();
            return game;
        }

        private Game CreateNewGame()
        {
            Game game = new Game();
            Chess chess = new Chess();
            game.FEN = chess.fen;
            game.status = "play";
            db.Games.Add(game);
            db.SaveChanges();
            return game;
        }

        public Game GetGame(int id)
        {
            return db.Games.Find(id);
        }

        public Game MakeMove(int id, string move)
        {
            Game game = GetGame(id);

            if (game == null) return game;
            if (game.status != "play") return game;

            Chess chess = new Chess(game.FEN);
            Chess chessNext = chess.Move(move);

            if (chessNext.fen == game.FEN) return game;

            game.FEN = chessNext.fen;
            if (chessNext.IsCheckMAT() || chessNext.IsCheckPAT())
                game.status = "done";

            db.Entry(game).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return game;
        }
    }
}