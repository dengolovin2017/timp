namespace ChessApi_server.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Side")]
    public partial class Side
    {
        public int id { get; set; }

        public int game_id { get; set; }

        public int player_id { get; set; }

        [Required]
        [StringLength(10)]
        public string color { get; set; }

        public int? points { get; set; }

        public int? draw { get; set; }

        public int? resign { get; set; }

        public virtual Game Game { get; set; }

        public virtual Player Player { get; set; }
    }
}
