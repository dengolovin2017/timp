namespace ChessApi_server.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Move")]
    public partial class Move
    {
        public int id { get; set; }

        public int game_id { get; set; }

        public int player_id { get; set; }

        public int ply { get; set; }

        [Required]
        [StringLength(255)]
        public string FEN { get; set; }

        [StringLength(255)]
        public string move_next { get; set; }

        public virtual Game Game { get; set; }

        public virtual Player Player { get; set; }
    }
}
