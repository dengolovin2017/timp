namespace ChessApi_server.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelChess : DbContext
    {
        public ModelChess()
            : base("name=ModelChess")
        {
        }

        public virtual DbSet<Game> Games { get; set; }
        public virtual DbSet<Move> Moves { get; set; }
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<Side> Sides { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Game>()
                .Property(e => e.FEN)
                .IsUnicode(false);

            modelBuilder.Entity<Game>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<Game>()
                .HasMany(e => e.Moves)
                .WithRequired(e => e.Game)
                .HasForeignKey(e => e.game_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Game>()
                .HasMany(e => e.Sides)
                .WithRequired(e => e.Game)
                .HasForeignKey(e => e.game_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Move>()
                .Property(e => e.FEN)
                .IsUnicode(false);

            modelBuilder.Entity<Move>()
                .Property(e => e.move_next)
                .IsUnicode(false);

            modelBuilder.Entity<Player>()
                .Property(e => e.login)
                .IsUnicode(false);

            modelBuilder.Entity<Player>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.Moves)
                .WithRequired(e => e.Player)
                .HasForeignKey(e => e.player_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.Sides)
                .WithRequired(e => e.Player)
                .HasForeignKey(e => e.player_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Side>()
                .Property(e => e.color)
                .IsUnicode(false);
        }
    }
}
